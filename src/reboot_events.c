/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "reboot.h"
#include "reboot_temperature.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <amxc/amxc.h>


// when cold boot reason is always "Power lost"
static void reboot_info_overload_reason_on_cold_boot(amxd_object_t* reboot_root_object, reboot_info_t* const reboot_info) {

    cstring_t bootcycle = NULL;
    amxc_string_t bootcycle_value;

    amxc_string_init(&bootcycle_value, 0);
    when_null(reboot_info, exit);
    when_null(reboot_root_object, exit);

    amxc_string_setf(&bootcycle_value, "%s" DM_REBOOT_CURRENT_BOOTCYCLE_NAME, reboot_get_vendor_prefix());
    bootcycle = amxd_object_get_value(cstring_t, reboot_root_object, amxc_string_get(&bootcycle_value, 0), NULL);
    when_null(bootcycle, exit);

    if(0 == strcmp("Cold", bootcycle)) {
        reboot_info_set_reason(reboot_info, REBOOT_REASON_POWER_LOST);
    }

exit:
    amxc_string_clean(&bootcycle_value);
    free(bootcycle);
    return;
}

static void reboot_info_fill_is_firmware_update(amxd_object_t* reboot_root_object, reboot_info_t* const reboot_info) {
    const char* reason = NULL;

    when_null(reboot_root_object, exit);

    reason = reboot_info_get_reason(reboot_info);
    if((NULL != reason) && (0 == strncmp(reason, REBOOT_REASON_FIRMWARE_UPGRADE, strlen(REBOOT_REASON_FIRMWARE_UPGRADE)))) {
        reboot_info_set_firmware_update(reboot_info, true);
    }
exit:
    return;
}

void add_newentry(amxd_object_t* object) {
    const cstring_t rbt_filename = GET_CHAR(amxo_parser_get_config(reboot_get_parser(), "rbt_reason_file"), NULL);
    const cstring_t rst_filename = GET_CHAR(amxo_parser_get_config(reboot_get_parser(), "rst_reason_file"), NULL);
    const cstring_t filename = rbt_filename;
    uint32_t boot_count = amxd_object_get_value(uint32_t, object, DM_REBOOT_BOOT_COUNT_NAME, NULL);
    reboot_info_t* reboot_info = reboot_info_new();

    when_null_trace(reboot_info, exit, ERROR, "Reboot failed to allocate reboot info")

    if(reboot_temperature_monitoring_init() != 0) {
        SAH_TRACEZ_WARNING(ME, "Reboot fails to monitor temperature");
    }

    // Manage the first boot, especially in cases where the persistent memory has been flushed (e.g '/cfg')
    if((boot_count == 0) && (filename != NULL) && (access(filename, F_OK) != 0)) {
        reboot_info_set_cause(reboot_info, REBOOT_CAUSE_LOCAL_FACTORY_RESET);
        reboot_info_setf_reason(reboot_info, REBOOT_REASON_INITIATED_BY_SOURCE, REBOOT_REASON_SOURCE_UNKNOWN);
        // Try to use a file stored in persistent memory that is not flushed (not affected by factory reset, e.g., /perm)
        filename = rst_filename;
    }

    // Parse the stored reboot file
    if(!STR_EMPTY(filename)) {
        reboot_file_parse(filename, reboot_info);
    } else {
        SAH_TRACEZ_WARNING(ME, "Reboot filename missing from config");
    }

    // Fetch data model information and, based on that, fill or overwrite the reboot info object
    reboot_info_fill_is_firmware_update(object, reboot_info);
    reboot_info_overload_reason_on_cold_boot(object, reboot_info);

    // Update the data model according to the reboot info object, including the Reboot and Reboot.Reboot.{i} object list
    if(reboot_dm_add_entry(reboot_info) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add reboot instance(cause = %s / reason = %s)",
                         reboot_info_get_cause(reboot_info), reboot_info_get_reason(reboot_info));
    } else {
        SAH_TRACEZ_INFO(ME, "Reboot instance(cause = %s / reason = %s) added",
                        reboot_info_get_cause(reboot_info), reboot_info_get_reason(reboot_info));
    }

exit:
    if((rst_filename != NULL) && (access(rst_filename, F_OK) == 0)) {
        remove(rst_filename);
    }
    if((rbt_filename != NULL) && (access(rbt_filename, F_OK) == 0)) {
        remove(rbt_filename);
    }

    reboot_info_delete(reboot_info);
}
