/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "reboot.h"
#include "reboot_temperature.h"

#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

static void handle_overheat(UNUSED const char* const event_name,
                            UNUSED const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    bool oh_status = GET_BOOL(event_data, "Status");
    const cstring_t filename = GET_CHAR(amxo_parser_get_config(reboot_get_parser(), "rbt_reason_file"), NULL);
    reboot_info_t* reboot_info = reboot_info_new();

    if(oh_status) {
        reboot_info_set_cause(reboot_info, REBOOT_CAUSE_LOCAL_REBOOT);
        reboot_info_set_reason(reboot_info, REBOOT_REASON_OVERHEAT);
        reboot_file_save(reboot_info);
    } else if(!STR_EMPTY(filename) && (access(filename, F_OK) == 0)) {
        const char* reason = NULL;
        reboot_file_parse(filename, reboot_info);
        reason = reboot_info_get_reason(reboot_info);
        if(!STR_EMPTY(reason) && (strcmp(reason, REBOOT_REASON_OVERHEAT) == 0)) {
            remove(filename);
        }
    }

    reboot_info_delete(reboot_info);
}

static void reboot_temperature_subscribe_alarm(amxb_bus_ctx_t* temp_ctx) {
    int rv = -1;

    when_null_trace(temp_ctx, exit, ERROR, "No TemperatureStatus bus context found");
    SAH_TRACEZ_INFO(ME, "TemperatureStatus object available, subscribe temperature alarm");
    rv = amxb_subscribe(temp_ctx, "TemperatureStatus.", "notification == 'HighTemperatureAlarm!'", handle_overheat, NULL);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "TemperatureStatus subscribription failed");
    }
exit:
    return;
}

static void reboot_temperature_subscribe_alarm_cb(UNUSED const char* signame,
                                                  UNUSED const amxc_var_t* const data,
                                                  UNUSED void* const priv) {

    reboot_temperature_subscribe_alarm(amxb_be_who_has("TemperatureStatus."));
}

int reboot_temperature_monitoring_init(void) {
    int rv = 0;
    amxb_bus_ctx_t* temp_ctx = amxb_be_who_has("TemperatureStatus.");

    if(temp_ctx == NULL) {
        SAH_TRACEZ_INFO(ME, "TemperatureStatus object isn't available, wait for it");
        amxb_wait_for_object("TemperatureStatus.");

        rv = amxp_slot_connect_filtered(NULL, "^wait:TemperatureStatus\\.$", NULL, reboot_temperature_subscribe_alarm_cb, NULL);
        if(rv != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to wait for TemperatureStatus to be available");
        }
    } else {
        reboot_temperature_subscribe_alarm(temp_ctx);
    }

    return rv;
}