/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "reboot.h"
#include "reboot_temperature.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <amxc/amxc.h>
#include <amxd/amxd_transaction.h>

static amxd_status_t reboot_dm_add_instance(amxd_object_t* object, const reboot_info_t* reboot_info) {
    amxd_status_t retval = amxd_status_unknown_error;
    amxc_ts_t ts;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    when_null(reboot_info, exit);
    amxc_ts_now(&ts);
    amxc_ts_to_local(&ts);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, object);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(amxc_ts_t, &trans, DM_REBOOT_INSTANCE_TIMESTAMP_NAME, &ts);
    amxd_trans_set_value(cstring_t, &trans, DM_REBOOT_INSTANCE_CAUSE_NAME, reboot_info_get_cause(reboot_info));
    amxd_trans_set_value(cstring_t, &trans, DM_REBOOT_INSTANCE_REASON_NAME, reboot_info_get_reason(reboot_info));
    amxd_trans_set_value(bool, &trans, DM_REBOOT_INSTANCE_FIRMWARE_UPDATED_NAME, reboot_info_is_firmware_update(reboot_info));
    retval = amxd_trans_apply(&trans, reboot_get_dm());

exit:
    amxd_trans_clean(&trans);
    return retval;
}

amxd_status_t reboot_dm_add_entry(reboot_info_t* reboot_info) {
    amxd_status_t status = amxd_status_invalid_arg;
    uint32_t boot_count = 0;
    uint32_t max_count = 0;
    amxd_object_t* obj_template = NULL;
    amxd_trans_t trans;
    amxc_string_t bootcycle_value;
    cstring_t bootcycle = NULL;
    const char* reason = NULL;
    amxd_object_t* object = amxd_dm_findf(reboot_get_dm(), DM_REBOOT_ROOT_PATH);

    amxc_string_init(&bootcycle_value, 0);

    when_null(reboot_info, exit);

    boot_count = amxd_object_get_value(uint32_t, object, DM_REBOOT_BOOT_COUNT_NAME, NULL);
    max_count = amxd_object_get_value(uint32_t, object, DM_REBOOT_MAX_REBOOT_ENTRIES_NAME, NULL);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(uint32_t, &trans, DM_REBOOT_BOOT_COUNT_NAME, boot_count + 1);
    reason = reboot_info_get_reason(reboot_info);

    if((NULL != reason)
       && (0 == strncmp(reason, REBOOT_REASON_HARDWARE_WATCHDOG, strlen(REBOOT_REASON_HARDWARE_WATCHDOG) - 3))) {
        uint32_t watchdog_count = amxd_object_get_value(uint32_t, object, DM_REBOOT_WATCHDOG_BOOT_COUNT_NAME, NULL);

        amxd_trans_set_value(uint32_t, &trans, DM_REBOOT_WATCHDOG_BOOT_COUNT_NAME, watchdog_count + 1);
    }

    amxc_string_setf(&bootcycle_value, "%s" DM_REBOOT_CURRENT_BOOTCYCLE_NAME, reboot_get_vendor_prefix());
    bootcycle = amxd_object_get_value(cstring_t, object, amxc_string_get(&bootcycle_value, 0), NULL);
    if(NULL != bootcycle) {
        if(0 == strcmp("Cold", bootcycle)) {
            uint32_t cold_boot_count = amxd_object_get_value(uint32_t, object, DM_REBOOT_COLD_BOOT_COUNT_NAME, NULL);

            amxd_trans_set_value(uint32_t, &trans, DM_REBOOT_COLD_BOOT_COUNT_NAME, cold_boot_count + 1);
        } else if(0 == strcmp("Warm", bootcycle)) {
            uint32_t warm_boot_count = amxd_object_get_value(uint32_t, object, DM_REBOOT_WARM_BOOT_COUNT_NAME, NULL);

            amxd_trans_set_value(uint32_t, &trans, DM_REBOOT_WARM_BOOT_COUNT_NAME, warm_boot_count + 1);
        }
    }

    status = amxd_trans_apply(&trans, reboot_get_dm());
    when_failed_trace(status, exit, ERROR, "Failed to update Reboot");

    obj_template = amxd_object_get_child(object, "Reboot");
    if(max_count == amxd_object_get_instance_count(obj_template)) {
        amxd_trans_t transaction;
        amxd_object_t* obj = amxc_container_of(amxd_object_first_instance(obj_template), amxd_object_t, it);

        amxd_trans_init(&transaction);
        amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
        amxd_trans_select_object(&transaction, obj_template);
        amxd_trans_del_inst(&transaction, amxd_object_get_index(obj), NULL);
        status = amxd_trans_apply(&transaction, reboot_get_dm());
        if(amxd_status_ok != status) {
            SAH_TRACEZ_WARNING(ME, "Failed to remove the last reboot reason (id=%u)", amxd_object_get_index(obj));
        }

        amxd_trans_clean(&transaction);
    }

    status = reboot_dm_add_instance(obj_template, reboot_info);

exit:
    amxd_trans_clean(&trans);
    amxc_string_clean(&bootcycle_value);
    free(bootcycle);
    return status;
}

amxd_status_t reboot_dm_remove_all_reboots(amxd_object_t* object) {

    amxd_status_t ret_status = amxd_status_unknown_error;
    amxd_object_t* obj_history = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_null(object, exit);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(uint32_t, &trans, DM_REBOOT_BOOT_COUNT_NAME, 0);
    amxd_trans_set_value(uint32_t, &trans, DM_REBOOT_WATCHDOG_BOOT_COUNT_NAME, 0);
    obj_history = amxd_object_get_child(object, "Reboot");
    amxd_trans_select_object(&trans, obj_history);
    amxd_object_for_each(instance, it, obj_history) {
        amxd_object_t* inst = amxc_llist_it_get_data(it, amxd_object_t, it);
        amxd_trans_del_inst(&trans, amxd_object_get_index(inst), NULL);
    }
    ret_status = amxd_trans_apply(&trans, reboot_get_dm());

exit:
    amxd_trans_clean(&trans);
    return ret_status;
}

amxd_status_t reboot_dm_remove_reboot(amxd_object_t* reboot) {
    amxd_status_t ret_status = amxd_status_ok;

    when_null_status(reboot, exit, ret_status = amxd_status_object_not_found);
    amxd_object_delete(&reboot);

exit:
    return ret_status;
}