/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "reboot.h"

#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <yajl/yajl_gen.h>
#include <amxj/amxj_variant.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

int reboot_file_parse(const cstring_t filename, reboot_info_t* const reboot_info) {
    int fd = -1;
    int ret = -1;
    const char* tmp = NULL;
    variant_json_t* reader = NULL;
    amxc_var_t* data = NULL;

    when_null(reboot_info, exit);
    when_failed_trace(amxj_reader_new(&reader), exit, ERROR, "Failed to create json file reader");

    fd = open(filename, O_RDONLY);
    when_true_trace(fd == -1, exit, WARNING, "Failed open file: %s", strerror(errno));

    amxj_read(reader, fd);
    data = amxj_reader_result(reader);

    tmp = GET_CHAR(data, DM_REBOOT_INSTANCE_CAUSE_NAME);
    if(!STR_EMPTY(tmp)) {
        when_failed_trace(reboot_info_set_cause(reboot_info, tmp), exit, ERROR, "Failed to set cause");
    }

    tmp = GET_CHAR(data, DM_REBOOT_INSTANCE_REASON_NAME);
    if(!STR_EMPTY(tmp)) {
        when_failed_trace(reboot_info_set_reason(reboot_info, tmp), exit, ERROR, "Failed to set cause");
    }

    ret = 0;
exit:

    if(reader) {
        amxj_reader_delete(&reader);
    }
    if(fd != -1) {
        close(fd);
    }

    amxc_var_delete(&data);

    return ret;
}

amxd_status_t reboot_file_save(const reboot_info_t* reboot_info) {
    int fd = -1;
    variant_json_t* writer = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    const cstring_t filename = NULL;
    amxc_var_t reboot_oject;
    const char* cause = NULL;
    const char* reason = NULL;


    amxc_var_init(&reboot_oject);
    amxc_var_set_type(&reboot_oject, AMXC_VAR_ID_HTABLE);
    cause = reboot_info_get_cause(reboot_info);
    reason = reboot_info_get_reason(reboot_info);

    if((NULL != cause)
       && ((strcmp(cause, REBOOT_CAUSE_LOCAL_FACTORY_RESET) == 0) || (strcmp(cause, REBOOT_CAUSE_REMOTE_FACTORY_RESET) == 0))) {
        filename = GET_CHAR(amxo_parser_get_config(reboot_get_parser(), "rst_reason_file"), NULL);
    } else {
        filename = GET_CHAR(amxo_parser_get_config(reboot_get_parser(), "rbt_reason_file"), NULL);
    }

    SAH_TRACEZ_INFO(ME, "Save Reboot(cause=%s/reason=%s) in %s", cause, reason, filename);

    amxc_var_add_new_key_cstring_t(&reboot_oject, DM_REBOOT_INSTANCE_CAUSE_NAME, cause);
    amxc_var_add_new_key_cstring_t(&reboot_oject, DM_REBOOT_INSTANCE_REASON_NAME, reason);

    when_failed_trace(amxj_writer_new(&writer, &reboot_oject), exit, ERROR, "Couldn't write json file");

    fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0644);
    when_true_trace(fd == -1, exit, ERROR, "Couldn't create reboot file to save");
    when_true_trace(amxj_write(writer, fd) <= 0, exit, ERROR, "Couldn't wrtie  reboot file");
    status = amxd_status_ok;

exit:
    amxc_var_clean(&reboot_oject);

    if(fd != -1) {
        close(fd);
    }
    if(writer != NULL) {
        amxj_writer_delete(&writer);
    }

    return status;
}