/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "reboot.h"
#include "reboot_rpc_calls.h"

#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <fcntl.h>
#include <errno.h>
#include <amxc/amxc.h>
#include <yajl/yajl_gen.h>
#include <amxj/amxj_variant.h>


amxd_status_t _reboot(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func,
                      amxc_var_t* args, UNUSED amxc_var_t* ret) {

    amxd_status_t ret_status;
    reboot_info_t* reboot_info = reboot_info_new();
    reboot_info_set_cause(reboot_info, GET_CHAR(args, DM_REBOOT_INSTANCE_CAUSE_NAME));
    reboot_info_set_reason(reboot_info, GET_CHAR(args, DM_REBOOT_INSTANCE_REASON_NAME));
    ret_status = reboot_file_save(reboot_info);
    reboot_info_delete(reboot_info);
    return ret_status;
}

amxd_status_t _reset(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func,
                     UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxd_status_t ret_status = amxd_status_function_not_implemented;
    return ret_status;
}

amxd_status_t _shutdown(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func,
                        UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxd_status_t ret_status = amxd_status_function_not_implemented;
    return ret_status;
}

amxd_status_t _RemoveAllReboots(amxd_object_t* object, UNUSED amxd_function_t* func, UNUSED amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {

    return reboot_dm_remove_all_reboots(object);
}

amxd_status_t _Reboot_Remove(amxd_object_t* reboot, UNUSED amxd_function_t* func, UNUSED amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    return reboot_dm_remove_reboot(reboot);
}

amxd_status_t _AddNewEntry(amxd_object_t* reboot,
                           UNUSED amxd_function_t* func,
                           UNUSED amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    add_newentry(reboot);
    return amxd_status_ok;
}
