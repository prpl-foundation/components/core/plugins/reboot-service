/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "reboot.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define REASON_MAX_LEN 256

struct _reboot_info {
    char* cause;
    char* reason;
    bool is_firmware_update;
};

reboot_info_t* reboot_info_new(void) {
    reboot_info_t* reboot = (reboot_info_t*) malloc(sizeof(reboot_info_t));

    when_null(reboot, exit);
    reboot->is_firmware_update = false;
    reboot->cause = strdup(REBOOT_CAUSE_LOCAL_REBOOT);
    when_null(reboot->cause, exit);
    reboot->reason = strdup(REBOOT_REASON_UNKNOWN);
    when_null(reboot->reason, exit);
exit:
    return reboot;
}

void reboot_info_delete(reboot_info_t* const reboot) {
    when_null(reboot, exit);
    free(reboot->cause);
    free(reboot->reason);
    free(reboot);
exit:
    return;
}

int reboot_info_set_firmware_update(reboot_info_t* const reboot, bool is_firmware_update) {
    int ret = -1;

    when_null(reboot, exit);
    reboot->is_firmware_update = is_firmware_update;
    ret = 0;
exit:
    return ret;
}

int reboot_info_set_cause(reboot_info_t* const reboot, const char* cause) {
    int ret = -1;

    when_null(reboot, exit);
    when_null(cause, exit);
    free(reboot->cause);
    reboot->cause = strdup(cause);
    when_null(reboot->cause, exit);
    ret = 0;
exit:
    return ret;
}

int reboot_info_set_reason(reboot_info_t* const reboot, const char* reason) {
    int ret = -1;

    when_null(reboot, exit);
    when_null(reason, exit);
    free(reboot->reason);
    reboot->reason = strdup(reason);
    when_null(reboot->reason, exit);
    ret = 0;
exit:
    return ret;
}

int reboot_info_setf_reason(reboot_info_t* const reboot, const char* fmt, ...) {
    int ret = -1;
    static char new_reason[REASON_MAX_LEN];

    when_null(reboot, exit);
    when_null(fmt, exit);

    va_list args;
    va_start(args, fmt);
    vsnprintf(new_reason, REASON_MAX_LEN, fmt, args);
    va_end(args);

    free(reboot->reason);

    reboot->reason = strdup(new_reason);
    when_null(reboot->reason, exit);
    ret = 0;
exit:
    return ret;
}

bool reboot_info_is_firmware_update(const reboot_info_t* const reboot) {
    return reboot != NULL ? reboot->is_firmware_update : false;
}

const char* reboot_info_get_cause(const reboot_info_t* const reboot) {
    return reboot != NULL ? reboot->cause : NULL;
}

const char* reboot_info_get_reason(const reboot_info_t* const reboot) {
    return reboot != NULL ? reboot->reason : NULL;
}
