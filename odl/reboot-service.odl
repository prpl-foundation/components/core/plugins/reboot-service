#include "global_amxb_timeouts.odl";
#include "mod_sahtrace.odl";

%config {
    name = "reboot-service";
    storage-type = "odl";
    storage-path = "${rw_data_path}/${name}";

    %global prefix_ = "X_PRPL-COM_";

    import-dirs = [
        ".",
        "${prefix}${plugin-dir}/${name}",
        "${prefix}${plugin-dir}/modules",
        "${prefix}/usr/lib/amx/${name}",
        "${prefix}/usr/local/lib/amx/modules"
    ];

    // persistent storage location
    import-dbg = false;
    odl = {
        load-dm-events = true,
        dm-load = true,
        dm-save = true,
        dm-save-on-changed = true,
        dm-save-delay = 1000,
        directory = "${storage-path}/odl",
        dm-defaults = "${name}_defaults.d/"
    };

    sahtrace = {
        type = "syslog",
        level = "${default_log_level}"
    };

    trace-zones = {
        "reboot" = "${default_trace_zone_level}"
    };

    pcm_svc_config = {
        "Objects" = "Reboot",
        "BackupFiles" = "reboot-service",
        "PostExport" = "AddNewEntry"
    };

    // main files
    definition_file = "${name}_definition.odl";
    rbt_reason_file = "/cfg/reboot_reason.json";
    rst_reason_file = "/perm/freset_reason.json";
}

import "${name}.so" as "${name}";
import "mod-dmext.so";

requires "Users.User.";

include "${definition_file}";
#include "${name}_caps.odl";
#include "mod_pcm_svc.odl";

%define {
    entry-point reboot-service.reboot_main;
}
