# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.0.1 - 2024-12-20(11:56:20 +0000)

### Fixes

- Reboot data model is not upgrade persistent

## Release v1.0.0 - 2024-12-10(11:50:21 +0000)

### Other

- - [prpl][reboot-service] Implement TR-181 2.18 Rename parameters/functions (break compatibility)

## Release v0.3.2 - 2024-10-21(12:36:42 +0000)

### Other

- [reboot-service] Some descriptions in the odl files do not reflect on the plugin's functionality

## Release v0.3.1 - 2024-10-10(10:15:27 +0000)

### Other

- - Reboot.X_PRPL-COM_CurrentBootCycle must not be persistent

## Release v0.3.0 - 2024-09-17(13:25:33 +0000)

## Release v0.2.8 - 2024-09-17(13:14:38 +0000)

### Other

- - [Reboot plugin] No logs visible in syslog

## Release v0.2.7 - 2024-09-17(10:08:49 +0000)

### Other

- [prpl][reboot-service] Implement TR-181 2.18 parameters/functions that don't break compatibility

## Release v0.2.6 - 2024-09-10(17:57:23 +0000)

### Other

- [reboot-service] Reboot service must depend on tr181-temperature

## Release v0.2.5 - 2024-09-05(14:01:49 +0000)

### Other

- Overwrite overheated reason in case of cold boot
- [reboot-service] Reboot service must depend on tr181-temperature

## Release v0.2.4 - 2024-07-30(14:30:05 +0000)

### Other

- Add tr181-device proxy odl files to various components

## Release v0.2.3 - 2024-07-30(09:10:55 +0000)

### Fixes

- Reboot data model is not upgrade persistent

## Release v0.2.2 - 2024-07-29(06:09:26 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v0.2.1 - 2024-07-08(07:09:27 +0000)

## Release v0.2.0 - 2024-07-02(09:51:15 +0000)

### New

- Set correct reboot reason in case of overheating

## Release v0.1.13 - 2024-06-27(10:14:38 +0000)

### Other

- amx plugin should not run as root user

## Release v0.1.12 - 2024-06-20(08:36:19 +0000)

### Fixes

- [reboot-service] Reboot Reason is "Unknown" for soft or hard reboot

## Release v0.1.11 - 2024-06-19(12:14:19 +0000)

### Other

- make available on gitlab.com

## Release v0.1.10 - 2024-04-10(10:00:59 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.1.9 - 2024-04-02(09:48:35 +0000)

### Fixes

- [Reboot Reason][button] Provide the correct Reboot reason, in case device is rebooted or reset by ResetButton

## Release v0.1.8 - 2024-03-28(12:38:13 +0000)

### Fixes

- CLONE - [Reboot Reason][button] Provide the correct Reboot reason, in case device is rebooted or reset by ResetButton

## Release v0.1.7 - 2024-03-18(08:42:55 +0000)

### Fixes

- [Reboot plugin] Unable to detect the boot reason automatically and the BootCounter is not incrementing

## Release v0.1.6 - 2024-03-15(14:10:56 +0000)

### Fixes

- [Safran_PRPL][Reboot plugin] Unable to detect the boot reason automatically and the BootCounter is not incrementing

## Release v0.1.5 - 2024-01-11(11:11:42 +0000)

### Other

- - [Reboot Service][amx][PRPL] Reboot plugin is not reboot persistent

## Release v0.1.4 - 2024-01-08(08:25:50 +0000)

### Fixes

- - Reboot plugin is not reboot persistent

## Release v0.1.3 - 2023-10-17(11:45:19 +0000)

### Fixes

- - [Reboot]Reboot api don't work as expected

## Release v0.1.2 - 2023-10-04(16:21:43 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v0.1.1 - 2023-09-25(10:02:32 +0000)

### Fixes

- - [reboot-service] plugin failed to start

## Release v0.1.0 - 2023-09-19(12:56:52 +0000)

### New

- - [Reboot]Port on prpl of nmc reboot

