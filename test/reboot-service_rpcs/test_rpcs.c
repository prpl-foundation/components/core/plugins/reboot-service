/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <signal.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_types.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_dm.h>

#include "test_rpcs.h"
#include "reboot.h"
#include "reboot_rpc_calls.h"

#include "../common/mock.h"
#include "../common/common_functions.h"

static const char* odl_config = "../common/mock.odl";
static const char* odl_nm = "../common/mock_deviceinfo.odl";
static const char* odl_temp = "../common/mock_temperature.odl";
static const char* odl_defs = "../../odl/reboot-service_definition.odl";

int test_setup(UNUSED void** state) {
    amxut_bus_setup(state);
    resolver_add_all_functions(amxut_bus_parser());
    amxut_dm_load_odl(odl_config);
    amxut_dm_load_odl(odl_defs);
    amxut_dm_load_odl(odl_nm);
    amxut_dm_load_odl(odl_temp);
    assert_int_equal(_reboot_main(AMXO_START, amxut_bus_dm(), amxut_bus_parser()), 0);

    amxut_bus_handle_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    _reboot_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser());
    amxut_bus_teardown(state);
    amxo_resolver_import_close_all();
    amxut_bus_handle_events();

    return 0;
}

#if 0 // used for debuging
static void dump(const char* path) {
    amxd_object_t* intf_obj = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    intf_obj = amxd_dm_findf(amxut_bus_dm(), path);
    assert_int_equal(amxd_object_invoke_function(intf_obj, "_get", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_dump(&ret, STDOUT_FILENO);
    amxc_var_clean(&ret);
}
#endif

static inline
amxc_llist_it_t* amxd_object_last_instance(const amxd_object_t* const object) {
    return object == NULL ? NULL : amxc_llist_get_last(&object->instances);
}

static uint32_t get_reboot_last_index(void) {
    amxd_object_t* obj_template = NULL;
    amxd_object_t* obj = NULL;

    obj_template = amxd_dm_findf(amxut_bus_dm(), "Reboot.Reboot");
    assert_non_null(obj_template);

    obj = amxc_container_of(amxd_object_last_instance(obj_template), amxd_object_t, it);
    assert_non_null(obj);
    return amxd_object_get_index(obj);
}

static cstring_t get_last_reason(void) {
    amxd_object_t* obj_template = NULL;
    amxd_object_t* obj = NULL;

    obj_template = amxd_dm_findf(amxut_bus_dm(), "Reboot.Reboot");
    assert_non_null(obj_template);

    obj = amxc_container_of(amxd_object_last_instance(obj_template), amxd_object_t, it);
    assert_non_null(obj);
    return amxd_object_get_value(cstring_t, obj, "Reason", NULL);
}

static cstring_t get_last_cause(void) {
    amxd_object_t* obj_template = NULL;
    amxd_object_t* obj = NULL;

    obj_template = amxd_dm_findf(amxut_bus_dm(), "Reboot.Reboot");
    assert_non_null(obj_template);

    obj = amxc_container_of(amxd_object_last_instance(obj_template), amxd_object_t, it);
    assert_non_null(obj);
    return amxd_object_get_value(cstring_t, obj, DM_REBOOT_INSTANCE_CAUSE_NAME, NULL);
}

static uint32_t get_reboot_count(void) {
    amxd_object_t* obj_template = NULL;

    obj_template = amxd_dm_findf(amxut_bus_dm(), "Reboot.Reboot");
    assert_non_null(obj_template);

    return amxd_object_get_instance_count(obj_template);
}

void test_call_reboot_rpc(UNUSED void** state) {
    amxd_object_t* intf_obj = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t reason_str;
    uint32_t max_count = 0;
    uint32_t boot_count = 0;
    uint32_t wd_count = 0;

    intf_obj = amxd_dm_findf(amxut_bus_dm(), "Reboot");
    assert_non_null(intf_obj);
    max_count = amxd_object_get_value(uint32_t, intf_obj, DM_REBOOT_MAX_REBOOT_ENTRIES_NAME, NULL);

    // this test permit to test the insert, the history limit, auto remove of the oldest element and finally remove all reboots
    for(int i = 0; i < 12; i++) {
        amxc_var_init(&ret);
        amxc_var_init(&args);
        amxc_string_init(&reason_str, 0);

        amxc_string_setf(&reason_str, REBOOT_REASON_HARDWARE_WATCHDOG, "plop");
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, DM_REBOOT_INSTANCE_CAUSE_NAME, REBOOT_CAUSE_LOCAL_REBOOT);
        amxc_var_add_key(cstring_t, &args, "Reason", amxc_string_get(&reason_str, 0));

        assert_int_equal(amxd_object_invoke_function(intf_obj, "reboot", &args, &ret), amxd_status_ok);
        _AddNewEntry(intf_obj, NULL, NULL, NULL);

        amxc_var_clean(&args);
        amxc_var_clean(&ret);
        amxc_string_clean(&reason_str);
        amxut_bus_handle_events();
    }

    boot_count = amxd_object_get_value(uint32_t, intf_obj, DM_REBOOT_BOOT_COUNT_NAME, NULL);
    wd_count = amxd_object_get_value(uint32_t, intf_obj, DM_REBOOT_WATCHDOG_BOOT_COUNT_NAME, NULL);

    assert_int_equal(get_reboot_last_index(), 12);
    assert_int_equal(get_reboot_count(), max_count);
    assert_int_equal(boot_count, 12);
    assert_int_equal(wd_count, 12);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    assert_int_equal(amxd_object_invoke_function(intf_obj, "RemoveAllReboots", &args, &ret), amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxut_bus_handle_events();

    boot_count = amxd_object_get_value(uint32_t, intf_obj, DM_REBOOT_BOOT_COUNT_NAME, NULL);
    wd_count = amxd_object_get_value(uint32_t, intf_obj, DM_REBOOT_WATCHDOG_BOOT_COUNT_NAME, NULL);

    assert_int_equal(get_reboot_count(), 0);
    assert_int_equal(boot_count, 0);
    assert_int_equal(wd_count, 0);
}

void test_defaults(UNUSED void** state) {
    amxd_object_t* intf_obj = NULL;
    cstring_t cause = NULL;
    cstring_t reason = NULL;
    amxc_string_t reason_str;
    amxc_var_t args;
    amxc_var_t ret;


    intf_obj = amxd_dm_findf(amxut_bus_dm(), "Reboot");
    assert_non_null(intf_obj);

    /* 1. Invoke reboot with no arguments */
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(intf_obj, "reboot", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    _AddNewEntry(intf_obj, NULL, NULL, NULL);
    cause = get_last_cause();
    reason = get_last_reason();
    assert_string_equal(cause, "RemoteReboot");
    assert_string_equal(reason, "Unknown");
    free(cause);
    free(reason);

    /* 2. Trigger a Remote reboot from TR069 */
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_string_init(&reason_str, 0);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_string_setf(&reason_str, REBOOT_REASON_INITIATED_BY_SOURCE, "WebUI");
    amxc_var_add_key(cstring_t, &args, DM_REBOOT_INSTANCE_CAUSE_NAME, REBOOT_CAUSE_LOCAL_REBOOT);
    amxc_var_add_key(cstring_t, &args, "Reason", amxc_string_get(&reason_str, 0));
    assert_int_equal(amxd_object_invoke_function(intf_obj, "reboot", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    _AddNewEntry(intf_obj, NULL, NULL, NULL);
    amxut_bus_handle_events();
    cause = get_last_cause();
    reason = get_last_reason();

    assert_string_equal(cause, "LocalReboot");
    assert_string_equal(reason, "Initiated by WebUI");

    amxc_string_clean(&reason_str);
    free(reason);
    free(cause);
}

void test_factory_reset(UNUSED void** state) {
    amxd_object_t* intf_obj = NULL;
    uint32_t boot_count = 0;
    cstring_t reason = NULL;
    cstring_t cause = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    intf_obj = amxd_dm_findf(amxut_bus_dm(), "Reboot");
    assert_non_null(intf_obj);

    /* 1. Check that we have clean slate */
    boot_count = amxd_object_get_value(uint32_t, intf_obj, DM_REBOOT_BOOT_COUNT_NAME, NULL);
    assert_int_equal(boot_count, 0);
    assert_int_equal(get_reboot_count(), 0);

    /* 2. After app_start there should be 1 Factory reset entry. */
    _AddNewEntry(intf_obj, NULL, NULL, NULL);
    amxut_bus_handle_events();
    boot_count = amxd_object_get_value(uint32_t, intf_obj, DM_REBOOT_BOOT_COUNT_NAME, NULL);
    assert_int_equal(boot_count, 1);
    assert_int_equal(get_reboot_last_index(), 1);
    assert_int_equal(get_reboot_count(), 1);
    cause = get_last_cause();
    reason = get_last_reason();

    assert_string_equal(cause, "LocalFactoryReset");
    assert_string_equal(reason, "Initiated by Unknown");
    free(cause);
    free(reason);

    /* 3. Perform FactoryReset and check again */
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(intf_obj, "RemoveAllReboots", &args, &ret), amxd_status_ok);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    amxut_bus_handle_events();
    boot_count = amxd_object_get_value(uint32_t, intf_obj, DM_REBOOT_BOOT_COUNT_NAME, NULL);

    assert_int_equal(get_reboot_count(), 0);
    assert_int_equal(boot_count, 0);

    amxc_var_init(&ret);
    amxc_var_init(&args);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, DM_REBOOT_INSTANCE_CAUSE_NAME, REBOOT_CAUSE_LOCAL_FACTORY_RESET);
    amxc_var_add_key(cstring_t, &args, "Reason", REBOOT_REASON_UNKNOWN);

    assert_int_equal(amxd_object_invoke_function(intf_obj, "reboot", &args, &ret), amxd_status_ok);
    _AddNewEntry(intf_obj, NULL, NULL, NULL);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxut_bus_handle_events();

    boot_count = amxd_object_get_value(uint32_t, intf_obj, DM_REBOOT_BOOT_COUNT_NAME, NULL);
    assert_int_equal(boot_count, 1);
    assert_int_equal(get_reboot_last_index(), 2);
    assert_int_equal(get_reboot_count(), 1);
    cause = get_last_cause();
    reason = get_last_reason();

    assert_string_equal(cause, "LocalFactoryReset");
    assert_string_equal(reason, "Unknown");

    free(cause);
    free(reason);
}

void test_random_reboot(UNUSED void** state) {
    amxd_object_t* intf_obj = NULL;
    uint32_t boot_count = 0;
    cstring_t cause = NULL;
    cstring_t reason = NULL;
    /* 1. perform some 'reboots' (several calls to app_start) */
    intf_obj = amxd_dm_findf(amxut_bus_dm(), "Reboot");
    assert_non_null(intf_obj);
    _AddNewEntry(intf_obj, NULL, NULL, NULL);
    _AddNewEntry(intf_obj, NULL, NULL, NULL);
    _AddNewEntry(intf_obj, NULL, NULL, NULL);

    /* 2. Check that bootcount increases (reason Unknown) */
    boot_count = amxd_object_get_value(uint32_t, intf_obj, DM_REBOOT_BOOT_COUNT_NAME, NULL);
    assert_int_equal(boot_count, 3);
    cause = get_last_cause();
    reason = get_last_reason();
    assert_string_equal(cause, "LocalReboot");
    assert_string_equal(reason, "Unknown");
    free(cause);
    free(reason);
}

void test_reboot_files(UNUSED void** state) {
    /* Check the existence of reboot-reason files at each stage of the
     * reboot-service */
    const cstring_t rbt_filename = GET_CHAR(amxo_parser_get_config(amxut_bus_parser(), "rbt_reason_file"), NULL);
    const cstring_t rst_filename = GET_CHAR(amxo_parser_get_config(amxut_bus_parser(), "rst_reason_file"), NULL);
    amxd_object_t* intf_obj = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t reason;

    intf_obj = amxd_dm_findf(amxut_bus_dm(), "Reboot");
    assert_non_null(intf_obj);

    /* 1. No file at boot */
    assert_int_not_equal(access(rbt_filename, F_OK), 0);
    assert_int_not_equal(access(rst_filename, F_OK), 0);

    /* 2. Call .reboot() with some args (other than FactoryReset), check
     * rbt_filename existance */
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, DM_REBOOT_INSTANCE_CAUSE_NAME, REBOOT_CAUSE_REMOTE_REBOOT);
    amxc_var_add_key(cstring_t, &args, "Reason", REBOOT_REASON_OVERHEAT);
    assert_int_equal(amxd_object_invoke_function(intf_obj, "reboot", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxut_bus_handle_events();
    assert_int_equal(access(rbt_filename, F_OK), 0);
    assert_int_not_equal(access(rst_filename, F_OK), 0);

    /* 3. Call .reboot() with FactoryReset arg, check rst_filename existance */
    amxc_string_init(&reason, 0);
    amxc_string_setf(&reason, REBOOT_REASON_INITIATED_BY_SOURCE, "WebUI");

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, DM_REBOOT_INSTANCE_CAUSE_NAME, REBOOT_CAUSE_LOCAL_FACTORY_RESET);
    amxc_var_add_key(cstring_t, &args, "Reason", amxc_string_get(&reason, 0));
    assert_int_equal(amxd_object_invoke_function(intf_obj, "reboot", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&reason);
    amxut_bus_handle_events();
    assert_int_equal(access(rbt_filename, F_OK), 0);    // still exists
    assert_int_equal(access(rst_filename, F_OK), 0);

    /* 4. Ensure app_start removes everything */
    _AddNewEntry(intf_obj, NULL, NULL, NULL);
    assert_int_not_equal(access(rbt_filename, F_OK), 0);
    assert_int_not_equal(access(rst_filename, F_OK), 0);
}

void test_reboot_overheat(UNUSED void** state) {
    /* Check the existence of reboot-reason file after overheating detection */
    const cstring_t rbt_filename = GET_CHAR(amxo_parser_get_config(amxut_bus_parser(), "rbt_reason_file"), NULL);
    amxd_object_t* intf_obj = NULL;
    amxd_object_t* temp_obj = NULL;
    amxc_var_t args;
    reboot_info_t* reboot_info = NULL;

    intf_obj = amxd_dm_findf(amxut_bus_dm(), "Reboot");
    assert_non_null(intf_obj);
    temp_obj = amxd_dm_findf(amxut_bus_dm(), "TemperatureStatus.");
    assert_non_null(temp_obj);

    /* 1. No file at boot */
    assert_int_not_equal(access(rbt_filename, F_OK), 0);

    /* 2. Start reboot-service */
    _AddNewEntry(intf_obj, NULL, NULL, NULL);

    /* 3. Emit an overheating on signal  and try catching it */
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "Status", true);
    amxd_object_emit_signal(temp_obj, "HighTemperatureAlarm!", &args);
    amxc_var_clean(&args);
    amxut_bus_handle_events();

    /* 4. Ensure that rbt file is created */
    assert_int_equal(access(rbt_filename, F_OK), 0);

    reboot_info = reboot_info_new();
    reboot_file_parse(rbt_filename, reboot_info);
    assert_string_equal(reboot_info_get_cause(reboot_info), REBOOT_CAUSE_LOCAL_REBOOT);
    assert_string_equal(reboot_info_get_reason(reboot_info), REBOOT_REASON_OVERHEAT);
    reboot_info_delete(reboot_info);

    /* 5. Emit an overheating off signal and try catching it */
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "Status", false);
    amxd_object_emit_signal(temp_obj, "HighTemperatureAlarm!", &args);
    amxc_var_clean(&args);
    amxut_bus_handle_events();

    /* 6. Ensure that rbt file is removed */
    assert_int_not_equal(access(rbt_filename, F_OK), 0);
}
