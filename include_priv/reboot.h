/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__REBOOT_H__)
#define __REBOOT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#define ME "reboot"


#define DM_REBOOT_ROOT_PATH "Reboot."

#define DM_REBOOT_MAX_REBOOT_ENTRIES_NAME "MaxRebootEntries"
#define DM_REBOOT_BOOT_COUNT_NAME "BootCount"
#define DM_REBOOT_WATCHDOG_BOOT_COUNT_NAME "WatchdogBootCount"
#define DM_REBOOT_CURRENT_BOOTCYCLE_NAME "CurrentBootCycle"
#define DM_REBOOT_COLD_BOOT_COUNT_NAME "ColdBootCount"
#define DM_REBOOT_WARM_BOOT_COUNT_NAME "WarmBootCount"

#define DM_REBOOT_INSTANCE_TIMESTAMP_NAME "TimeStamp"
#define DM_REBOOT_INSTANCE_CAUSE_NAME "Cause"
#define DM_REBOOT_INSTANCE_REASON_NAME "Reason"
#define DM_REBOOT_INSTANCE_FIRMWARE_UPDATED_NAME "FirmwareUpdated"

#define REBOOT_CAUSE_LOCAL_REBOOT "LocalReboot"
#define REBOOT_CAUSE_REMOTE_REBOOT "RemoteReboot"
#define REBOOT_CAUSE_FACTORY_RESET "FactoryReset"
#define REBOOT_CAUSE_LOCAL_FACTORY_RESET "LocalFactoryReset"
#define REBOOT_CAUSE_REMOTE_FACTORY_RESET "RemoteFactoryReset"

#define REBOOT_REASON_FIRMWARE_UPGRADE "Firmware Upgrade"
#define REBOOT_REASON_FIRMWARE_DOWNGRADE "Firmware Downgrade"
#define REBOOT_REASON_POWER_LOST "Power lost"
#define REBOOT_REASON_OVERHEAT "Overheat"
#define REBOOT_REASON_AUTOMATIC_PLANNED_REBOOT "Automatic planned reboot"
#define REBOOT_REASON_USERSPACE_CRASH "Userspace crash in %s %s"                                // [process] [pid]
#define REBOOT_REASON_KERNEL_CRASH "Kernel crash %s"                                            // [source]
#define REBOOT_REASON_HARDWARE_WATCHDOG "Hardware watchdog %s"                                  // [source]
#define REBOOT_REASON_CRASH_LOOP_PROTECTION_DETECTION "Crash loop Protection detected on %s %s" // [process] [pid]
#define REBOOT_REASON_INITIATED_BY_SOURCE "Initiated by %s"                                     // [source]
#define REBOOT_REASON_UNKNOWN "Unknown"

#define REBOOT_REASON_SOURCE_UNKNOWN "Unknown"


#define STR_EMPTY(_x) ((_x) == NULL || (_x)[0] == '\0')

typedef struct _reboot_info reboot_info_t;

amxd_dm_t* reboot_get_dm(void);
amxo_parser_t* reboot_get_parser(void);
amxb_bus_ctx_t* reboot_get_ctx(void);
const cstring_t reboot_get_vendor_prefix(void);
int _reboot_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);

void add_newentry(amxd_object_t* object);
reboot_info_t* reboot_info_new(void);
void reboot_info_delete(reboot_info_t* const reboot);
int reboot_info_set_cause(reboot_info_t* const reboot, const char* cause);
int reboot_info_set_reason(reboot_info_t* const reboot, const char* reason);
int reboot_info_setf_reason(reboot_info_t* const reboot, const char* fmt, ...);
const char* reboot_info_get_cause(const reboot_info_t* const reboot);
const char* reboot_info_get_reason(const reboot_info_t* const reboot);
bool reboot_info_is_firmware_update(const reboot_info_t* const reboot);
int reboot_info_set_firmware_update(reboot_info_t* const reboot, bool is_firmware_update);

amxd_status_t reboot_dm_add_entry(reboot_info_t* reboot_info);
amxd_status_t reboot_dm_remove_all_reboots(amxd_object_t* object);
amxd_status_t reboot_dm_remove_reboot(amxd_object_t* reboot);

amxd_status_t reboot_file_save(const reboot_info_t* reboot_info);
int reboot_file_parse(const cstring_t filename, reboot_info_t* const reboot_info);
#ifdef __cplusplus
}
#endif

#endif // __REBOOT_H__
